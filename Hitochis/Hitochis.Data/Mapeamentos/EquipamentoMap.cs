﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class EquipamentoMap : EntityTypeConfiguration<Equipamento>
	{
		public EquipamentoMap()
		{
			ToTable("Equipamento");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Nome).HasColumnName("Nome");
			Property(x => x.Descricao).HasColumnName("Descricao");
			Property(x => x.BonusBase).HasColumnName("BonusBase");
			Property(x => x.BonusPorNivel).HasColumnName("BonusPorNivel");
			Property(x => x.LevelNecessario).HasColumnName("LevelNecessario");

			Property(x => x.TipoEquipamentoId).HasColumnName("TipoEquipamentoId");
				HasRequired(x => x.TipoEquipamento).WithMany().HasForeignKey(x => x.TipoEquipamentoId);
		}
	}
}
