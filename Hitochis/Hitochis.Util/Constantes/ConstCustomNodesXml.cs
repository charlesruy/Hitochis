﻿namespace Hitochis.Util.Constantes
{
	public static class ConstCustomNodesXml
    {
        #region | Nodes |

        public const string CHAPTER = "chapter";
        public const string DIALOG = "dialog";
        public const string CHARACTER = "character";
        public const string SPEAK = "speak";
        public const string MUSIC = "music";
        public const string BATTLE = "battle";
        public const string COMPLETE = "complete";
        public const string GAMEOVER = "gameover";

        #endregion

        #region | Attributes |

        public const string ID = "id";
        public const string NAME = "name";
        public const string LOCATION = "location";
        public const string FACIAL_EXPRESSION = "facial-expression";
        public const string REPEAT = "repeat";
        public const string CASE = "case";

        #endregion

        #region | Values |

        public const string TRUE = "true";
        public const string FALSE = "false";
        public const string WIN = "win";
        public const string LOSE = "lose";

        #endregion
    }
}
