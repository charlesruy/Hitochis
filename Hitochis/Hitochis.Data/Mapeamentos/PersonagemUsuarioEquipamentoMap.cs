﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class PersonagemUsuarioEquipamentoMap : EntityTypeConfiguration<PersonagemUsuarioEquipamento>
	{
		public PersonagemUsuarioEquipamentoMap()
		{
			ToTable("PersonagemUsuarioEquipamento");

			HasKey(x => new { x.PersonagemId, x.UsuarioEquipamentoId });

			Property(x => x.PersonagemId).HasColumnName("PersonagemId");
			HasRequired(x => x.Personagem).WithMany(y => y.Equipamentos).HasForeignKey(x => x.PersonagemId);

			Property(x => x.UsuarioEquipamentoId).HasColumnName("UsuarioEquipamentoId");
			HasRequired(x => x.UsuarioEquipamento).WithMany().HasForeignKey(x => x.UsuarioEquipamentoId);
		}
	}
}
