﻿using System.Collections.ObjectModel;

namespace Hitochis.Util.Constantes
{
	public static class ConstAtributos
    {
        #region | Atributos |

        public const string HP = "HP";
        public const string MP = "MP";
        public const string ATTACK = "Attack";
        public const string DEFENSE = "Defense";
        public const string WISDOM = "Wisdom";
        public const string MAGIC_DEFENSE = "MagicDefense";
        public const string ACCURACY = "Accuracy";
        public const string EVADE = "Evade";
        public const string SPEED = "Speed";

        #endregion

        #region | Lista de Tipos de Atributos |

        public static ReadOnlyCollection<string> TIPOS_ATRIBUTOS = new ReadOnlyCollection<string>(new[]
        {
            ConstAtributos.HP,
            ConstAtributos.MP,
            ConstAtributos.ATTACK,
            ConstAtributos.DEFENSE,
            ConstAtributos.WISDOM,
            ConstAtributos.MAGIC_DEFENSE,
            ConstAtributos.ACCURACY,
            ConstAtributos.EVADE,
            ConstAtributos.SPEED
        });

        #endregion
    }
}
