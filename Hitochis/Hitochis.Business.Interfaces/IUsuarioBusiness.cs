﻿using Hitochis.Entities.Classes;

namespace Hitochis.Business.Interfaces
{
	public interface IUsuarioBusiness<T>
    {
        Usuario CriarConta(string login, string senha);

        Usuario Autenticar(string login, string senha);
    }
}
