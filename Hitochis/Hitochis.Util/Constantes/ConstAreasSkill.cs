﻿namespace Hitochis.Util.Constantes
{
	public static class ConstAreasSkill
    {
        #region | Atributos |

        public const string SINGLE_TARGET = "SingleTarget";
        public const string AREA_OF_EFFECT = "AreaOfEffect";

        #endregion
    }
}
