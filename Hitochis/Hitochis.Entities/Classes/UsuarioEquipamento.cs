﻿namespace Hitochis.Entities.Classes
{
	public class UsuarioEquipamento
	{
		public long Id { get; set; }
		public int Nivel { get; set; }

		public long UsuarioId { get; set; }
		public Usuario Usuario { get; set; }

		public int EquipamentoId { get; set; }
		public Equipamento Equipamento { get; set; }

		protected UsuarioEquipamento() { }

		public UsuarioEquipamento(long id, int nivel, long usuarioId, int equipamentoId)
		{
			Id = id;
			Nivel = nivel;
			UsuarioId = usuarioId;
			EquipamentoId = equipamentoId;
		}
	}
}
