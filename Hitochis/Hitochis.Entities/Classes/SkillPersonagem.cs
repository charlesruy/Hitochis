﻿namespace Hitochis.Entities.Classes
{
	public class SkillPersonagem
    {
        public long Id { get; set; }
		public int Nivel { get; set; }

		public int SkillId { get; set; }
        public Skill Skill { get; set; }

		public long PersonagemId { get; set; }
        public Personagem Personagem { get; set; }

		protected SkillPersonagem() { }


		public SkillPersonagem(long id, int nivel, int skillId, long personagemId)
		{
			Id = id;
			Nivel = nivel;
			SkillId = skillId;
			PersonagemId = personagemId;
		}
	}
}
