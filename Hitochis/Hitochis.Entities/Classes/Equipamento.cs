﻿using System.Collections.Generic;

namespace Hitochis.Entities.Classes
{
	public class Equipamento
    {
		public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int BonusBase { get; set; }
        public int BonusPorNivel { get; set; }
        public int LevelNecessario { get; set; }

		public int TipoEquipamentoId { get; set; }
		public TipoEquipamento TipoEquipamento { get; set; }

		public List<UsuarioEquipamento> EquipamentosUsuarios { get; set; }

		protected Equipamento() { }

		public Equipamento(int id, string nome, string descricao, int bonusBase, int bonusPorNivel, int levelNecessario, int tipoEquipamentoId)
		{
			Id = id;
			Nome = nome;
			Descricao = descricao;
			BonusBase = bonusBase;
			BonusPorNivel = bonusPorNivel;
			LevelNecessario = levelNecessario;
			TipoEquipamentoId = tipoEquipamentoId;
		}
	}
}
