﻿using System.Collections.ObjectModel;

namespace Hitochis.Util.Constantes
{
	public static class ConstPersonagens
    {
        #region | Atributos |

        public const string KENJI = "KENJI";
        public const string YOSHINY = "YOSHINY";

        #endregion

        #region | Lista de Tipos de Atributos |

        public static ReadOnlyCollection<string> PERSONAGENS = new ReadOnlyCollection<string>(new[]
        {
            ConstPersonagens.KENJI,
            ConstPersonagens.YOSHINY
        });

        #endregion
    }
}
