﻿using Hitochis.Data.Interfaces;
using Hitochis.Entities.Classes;
using System;
using System.Linq;

namespace Hitochis.Data
{
	public class UsuarioData<T> : IUsuarioData<T>
    {
        #region | Métodos Públicos |

        #region | Criar Conta |

        public Usuario CriarConta(Usuario newUser)
        {
            if (!VerificarExisteLogin(newUser.Login))
            {
                using (var ctx = new HitochisContext())
                {
                    ctx.Usuarios.Add(newUser);
                    ctx.SaveChanges();
                }
                return Autenticar(newUser.Login, newUser.Senha);
            }
            throw new Exception("Este login já existe..!");
        }

        #endregion

        #region | Autenticar |

        public Usuario Autenticar(string login, string senha)
        {
            using (var ctx = new HitochisContext())
            {
                return ctx.Usuarios
                          .SingleOrDefault(x => x.Login.ToLower() == login.ToLower() && x.Senha == senha);
            }
        }

        #endregion

        #endregion

        #region | Métodos Privados |

        private bool VerificarExisteLogin(string login)
        {
            using (var ctx = new HitochisContext())
            {
                return ctx.Usuarios.Any(x => x.Login == login);
            }
        }

        #endregion
    }
}
