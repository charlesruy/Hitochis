﻿using System.Collections.ObjectModel;

namespace Hitochis.Util.Constantes
{
	public static class ConstTiposSkill
    {
        #region | Atributos |

        //Damage
        public const string ONLY_DAMAGE = "Only Damage";
        public const string DAMAGE_BUFF = "Damage Buff";
        public const string DAMAGE_DEBUFF = "Damage Debuff";

        //Status
        public const string ONLY_BUFF = "Only Buff";
        public const string ONLY_DEBUFF = "Only Debuff";
        public const string BUFF_DEBUFF = "Buff Debuff";
        
        //Support
        public const string HEAL = "Heal";
        public const string DISPEL_MAGIC = "Dispel Magic";

        #endregion

        #region | Lista de Tipos de Atributos |

        public static ReadOnlyCollection<string> TIPOS_SKILL = new ReadOnlyCollection<string>(new[]
        {
            ConstTiposSkill.ONLY_DAMAGE,
            ConstTiposSkill.DAMAGE_BUFF,
            ConstTiposSkill.DAMAGE_DEBUFF,
            ConstTiposSkill.ONLY_BUFF,
            ConstTiposSkill.ONLY_DEBUFF,
            ConstTiposSkill.BUFF_DEBUFF,
            ConstTiposSkill.HEAL,
            ConstTiposSkill.DISPEL_MAGIC
        });

        #endregion
    }
}
