﻿using Hitochis.Entities.Classes;
using Hitochis.Util.Constantes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Hitochis.Entities.HelperClasses
{
	public static class CalculosBatalha
    {
        #region | Ataque Padrão |

        public static string ExecutarAtaquePadrao(ref Personagem characterTurn, ref Personagem characterTarget)
        {
            if (characterTurn.TipoAtributoAtaquePadrao == ConstTipoAtributoAtaque.ATAQUE_FISICO)
            {
                /* Se o Accuracy do Jogador Atual for o dobro do Evade do Target, não tem chance de esquiva (apenas para Dano Físico) */
                if ((int)characterTurn.AccuracyAtual <= (int)characterTarget.EvadeAtual * 2)
                {
                    int ramdomAccuracy = new Random().Next(0, (int)characterTurn.AccuracyAtual);
                    int ramdomEvade = new Random().Next(0, (int)characterTarget.EvadeAtual);

                    /* Caso o ramdomEvade for maior ou igual ao ramdomAccuracy, o Jogador Target esquivou do ataque */
                    if (ramdomEvade >= ramdomAccuracy)
                        return "Esquivou";
                }

                int ramdomAttack = new Random().Next(0, (int)characterTurn.AttackAtual);
                int ramdomDefense = new Random().Next(0, (int)characterTarget.DefenseAtual);

				// !important: Mínimo de dano causado é 10

                /* Se o ramdomAttack for o dobro da ramdomDefense, o Jogador Atual causa um Critical Hit */
                if (ramdomAttack >= (ramdomDefense * 2))
                {
                    /* Fórmula de Critical Hit = Dano Calculado + AttackAtual do Jogador Atual */
                    int danoCausado = ((ramdomAttack - ramdomDefense) + (int)characterTurn.AttackAtual);
					danoCausado = danoCausado > 10 ? danoCausado : 10;
                    characterTarget.HPAtual -= danoCausado;
                    return "Dano causado = " + danoCausado;
                }
                /* Se o ramdomAttack for maior que a ramdomDefense, o Jogador Atual causa um Dano Físico Padrão */
                else if ((ramdomAttack - ramdomDefense) > 0)
                {
                    /* Fórmula de Dano Físico Padrão = Dano Calculado */
                    int danoCausado = (ramdomAttack - ramdomDefense);
					danoCausado = danoCausado > 10 ? danoCausado : 10;
                    characterTarget.HPAtual -= danoCausado;
                    return "Ataque efetuado = " + danoCausado;
                }
                /* Caso o ramdomAttack for menor ou igual ao ramdomDefense, o Jogador Target defendeu o ataque */
                else
                {
                    return "Bloqueou";
                }
            }
            else if (characterTurn.TipoAtributoAtaquePadrao == ConstTipoAtributoAtaque.ATAQUE_MAGICO)
            {
                int ramdomWisdom = new Random().Next(0, (int)characterTurn.WisdomAtual);
                int ramdomMagicDefense = new Random().Next(0, (int)characterTarget.MagicDefenseAtual);

                /* Ataques Mágicos não tem Evade e nem Block. Por isso a fórmula é a seguinte: */
                /* Fórmula de Dano Mágico Padrão = Dano Calculado minimo é 10 */
                int danoCausado = (ramdomWisdom - ramdomMagicDefense) > 10 ? (ramdomWisdom - ramdomMagicDefense) : 10;
                characterTarget.HPAtual -= danoCausado;
                return "Dano de Magia = " + danoCausado;
            }

            return string.Empty;
        }

        #endregion

        #region | Executar Skill |

        public static void ExecutarSkill(Skill skillUtilizada, int nivelSkill, Personagem characterTurn, Personagem characterTarget, 
			string positionTurn, PictureBox picBoxTurn, ProgressBar lifebarTurn, Label lblHpMaxTurn, Label lblHpAtualTurn, Label lblMpMaxTurn, Label lblMpAtualTurn,
			string positionTarget, PictureBox picBoxTarget, ProgressBar lifebarTarget, Label lblHpMaxTarget, Label lblHpAtualTarget, Label lblMpMaxTarget, Label lblMpAtualTarget)
        {
            switch (skillUtilizada.TipoSkill)
            {
                case ConstTiposSkill.ONLY_DAMAGE:
					OnlyDamage(skillUtilizada, nivelSkill, characterTurn, characterTarget);
					break;
                case ConstTiposSkill.DAMAGE_BUFF:
                    break;
                case ConstTiposSkill.DAMAGE_DEBUFF:
                    break;
                case ConstTiposSkill.ONLY_BUFF:
                    break;
                case ConstTiposSkill.ONLY_DEBUFF:
                    break;
                case ConstTiposSkill.BUFF_DEBUFF:
                    break;
                case ConstTiposSkill.HEAL:
                    Heal(nivelSkill, skillUtilizada, characterTurn, characterTarget);
                    break;
                case ConstTiposSkill.DISPEL_MAGIC:
                    DispelMagic(nivelSkill, skillUtilizada, characterTurn, characterTarget);
                    break;
            }
        }

        #endregion

        #region | Only Damage |

        private static string OnlyDamage(Skill skillUtilizada, int nivelSkill, Personagem characterTurn, Personagem characterTarget)
        {
            /* Seta a Quantidade Máxima de dano que a skill pode causar */
            int quantidadeMaxDano = (int)skillUtilizada.DanoBase + (nivelSkill * (int)skillUtilizada.DanoPorNivel) + (int)characterTurn.WisdomAtual;

            int ramdomDamage = new Random().Next(0, quantidadeMaxDano);
			int ramdomMagicDefense = new Random().Next(0, (int)characterTarget.MagicDefenseAtual);

			int danoReal = (ramdomDamage - ramdomMagicDefense) > (int)skillUtilizada.DanoBase ? (ramdomDamage - ramdomMagicDefense) : (int)skillUtilizada.DanoBase;
			danoReal = danoReal > 10 ? danoReal : 10;

			characterTarget.HPAtual -= danoReal;
            return "Damage = " + danoReal;
        }

        #endregion

        #region | Heal |

        private static void Heal(int nivelSkill, Skill skillUtilizada, Personagem characterTurn, Personagem characterTarget)
        {
            /* Seta a Quantidade Máxima que a skill pode curar */
            int quantidadeMaxCura = (int)skillUtilizada.DanoBase + (nivelSkill * (int)skillUtilizada.DanoPorNivel) + (int)characterTurn.WisdomAtual;
            /* Seta a metade dessa cura para ser a Quantidade Mímina de Cura */
            int quantidadeMinCura = Convert.ToInt32(quantidadeMaxCura / 2);
            /* Seta a quantidade necessária para Crítico da Cura (90% da quantidade máxima) */
            int quantidadeCritico = Convert.ToInt32(quantidadeMaxCura * 0.9);

            /* Calcula o ramdomHeal */
            int ramdomHeal = new Random().Next(quantidadeMinCura, quantidadeMaxCura);

            /* Caso o ramdomHeal for maior ou igual a quantidade necessária para Crítico da Cura, o ramdomHeal é dobrado */
            if (ramdomHeal >= quantidadeCritico)
                ramdomHeal *= 2;
			
            characterTarget.HPAtual += ramdomHeal;
            //return "Cura = " + ramdomHeal;
        }

        #endregion

        #region | DispelMagic |

        private static void DispelMagic(int nivelSkill, Skill skillUtilizada, Personagem characterTurn, Personagem characterTarget)
        {
			if (characterTarget.StatusTemporarios.Any())
			{
				if (characterTarget.StatusTemporarios.Count > nivelSkill)
					characterTarget.StatusTemporarios.RemoveRange(characterTarget.StatusTemporarios.Count - 1 - nivelSkill, nivelSkill);
				else
					characterTarget.StatusTemporarios.Clear();
			}
        }

        #endregion
    }
}
