﻿using System.Collections.Generic;

namespace Hitochis.Entities.Classes
{
	public class Usuario
    {
		public long Id { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string FezPrologo { get; set; }
        public string Historia { get; set; }
        public long Money { get; set; }

        public List<Personagem> Personagens { get; set; }
		public List<UsuarioEquipamento> BolsaItens { get; set; }

		protected Usuario() { }

		public Usuario(long id, string login, string senha, string fezPrologo, string historia, long money)
		{
			Id = id;
			Login = login;
			Senha = senha;
			FezPrologo = fezPrologo;
			Historia = historia;
			Money = money;
		}

		public Usuario(string login, string senha, string fezPrologo, string historia, long money)
		{
			Login = login;
			Senha = senha;
			FezPrologo = fezPrologo;
			Historia = historia;
			Money = money;
		}
	}
}
