﻿using Hitochis.Entities.Classes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class UsuarioMap : EntityTypeConfiguration<Usuario>
	{
		public UsuarioMap()
		{
			ToTable("Usuario");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			Property(x => x.Login).HasColumnName("Login");
			Property(x => x.Senha).HasColumnName("Senha");
			Property(x => x.FezPrologo).HasColumnName("FezPrologo");
			Property(x => x.Historia).HasColumnName("Historia");
			Property(x => x.Money).HasColumnName("Money");
		}
	}
}
