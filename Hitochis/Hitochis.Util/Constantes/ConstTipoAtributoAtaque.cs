﻿namespace Hitochis.Util.Constantes
{
	public static class ConstTipoAtributoAtaque
    {
        #region | Atributos |

        public const string ATAQUE_FISICO = "AtaqueFisico";
        public const string ATAQUE_MAGICO = "AtaqueMagico";

        #endregion
    }
}
