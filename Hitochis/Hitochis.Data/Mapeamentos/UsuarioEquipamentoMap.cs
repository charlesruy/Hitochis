﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class UsuarioEquipamentoMap : EntityTypeConfiguration<UsuarioEquipamento>
	{
		public UsuarioEquipamentoMap()
		{
			ToTable("UsuarioEquipamento");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Nivel).HasColumnName("Nivel");

			Property(x => x.UsuarioId).HasColumnName("UsuarioId");
				HasRequired(x => x.Usuario).WithMany(y => y.BolsaItens).HasForeignKey(x => x.UsuarioId);

			Property(x => x.EquipamentoId).HasColumnName("EquipamentoId");
				HasRequired(x => x.Equipamento).WithMany(y => y.EquipamentosUsuarios).HasForeignKey(x => x.EquipamentoId);
		}
	}
}
