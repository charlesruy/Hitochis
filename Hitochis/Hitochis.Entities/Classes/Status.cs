﻿namespace Hitochis.Entities.Classes
{
	public class Status
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Operacao { get; set; }
        public string TipoAtributo { get; set; }
        public double Valor { get; set; }
        public bool JaCalculado { get; set; }
        public int RodadasRestantes { get; set; }
    }
}
