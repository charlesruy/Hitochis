﻿using Hitochis.Data.Mapeamentos;
using Hitochis.Entities.Classes;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Hitochis.Data
{
	public class HitochisContext : DbContext
    {
        #region | Propriedades - DBSets |

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Personagem> Personagens { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillPersonagem> SkillsPersonagens { get; set; }
		public DbSet<TipoEquipamento> TiposEquipamentos { get; set; }
		public DbSet<Equipamento> Equipamentos { get; set; }
		public DbSet<UsuarioEquipamento> UsuariosEquipamentos { get; set; }
		public DbSet<PersonagemUsuarioEquipamento> PersonagensUsuariosEquipamentos { get; set; }

		//public const string filename = @"C:\Users\charles.ruy\Desktop\Hitochis.db";
		//: base(new SQLiteConnection()
		//    {
		//        ConnectionString = new SQLiteConnectionStringBuilder()
		//        {
		//            DataSource = filename,
		//            ForeignKeys = true,
		//        }.ConnectionString }, true)

		#endregion

		#region | Construtor |

		public HitochisContext() : base("HitochisContext")
        {
            Database.SetInitializer<HitochisContext>(null);
        }

        #endregion

        #region | OnModelCreating |

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ForeignKeyNavigationPropertyAttributeConvention>();

			modelBuilder.Configurations.Add(new UsuarioMap());
			modelBuilder.Configurations.Add(new TipoEquipamentoMap());
			modelBuilder.Configurations.Add(new EquipamentoMap());
			modelBuilder.Configurations.Add(new UsuarioEquipamentoMap());
			modelBuilder.Configurations.Add(new SkillMap());
			modelBuilder.Configurations.Add(new PersonagemMap());
			modelBuilder.Configurations.Add(new SkillPersonagemMap());
			modelBuilder.Configurations.Add(new PersonagemUsuarioEquipamentoMap());

			//modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			//modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
			//modelBuilder.Configurations.AddFromAssembly(typeof(HitochisContext).Assembly);

			//Exemplo de Mapeamento - Many to Many

			//modelBuilder.Entity<Skill>()
			//    .HasMany<BuffDebuff>(x => x.Skills)
			//    .WithMany(y => y.Personagens)
			//    .Map(xy =>
			//    {
			//        xy.MapLeftKey("IDPersonagem");
			//        xy.MapRightKey("IDSkill");
			//        xy.ToTable("PersonagemSkill");
			//    });
		} 

        #endregion
    }
}
