﻿namespace Hitochis.Entities.Classes
{
	public class PersonagemUsuarioEquipamento
	{
		public long UsuarioEquipamentoId { get; set; }
		public UsuarioEquipamento UsuarioEquipamento { get; set; }

		public long PersonagemId { get; set; }
		public Personagem Personagem { get; set; }

		protected PersonagemUsuarioEquipamento() { }

		public PersonagemUsuarioEquipamento(long usuarioEquipamentoId, long personagemId)
		{
			UsuarioEquipamentoId = usuarioEquipamentoId;
			PersonagemId = personagemId;
		}
	}
}
