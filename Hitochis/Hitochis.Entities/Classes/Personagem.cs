﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hitochis.Entities.Classes
{
	public class Personagem
	{
		public long Id { get; set; }
		public string Nome { get; set; }
		public int Level { get; set; }
		public string Descricao { get; set; }
		public string TipoAtributoAtaquePadrao { get; set; }

		//Atributos base do personagem
		public int HPBase { get; set; } //Health Points
		public int MPBase { get; set; } //Mana Points
		public int AttackBase { get; set; } // Dano Físico
		public int DefenseBase { get; set; } // Defesa p/ Dano Físico
		public int WisdomBase { get; set; } // Dano Mágico
		public int MagicDefenseBase { get; set; } // Defesa p/ Dano Mágico
		public int AccuracyBase { get; set; } // Taxa de Acerto p/ Dano Físico
		public int EvadeBase { get; set; } // Taxa de Esquiva p/ Dano Físico
		public int SpeedBase { get; set; } // Velocidade

		//Atributos totais do personagem (Base + Equipamento)
		public int HP { get; set; }
		public int MP { get; set; }
		public int Attack { get; set; }
		public int Defense { get; set; }
		public int Wisdom { get; set; }
		public int MagicDefense { get; set; }
		public int Accuracy { get; set; }
		public int Evade { get; set; }
		public int Speed { get; set; }
		public double HPAtual { get; set; }
		public double MPAtual { get; set; }

		// Lista para guardar as skills do personagem
		public List<SkillPersonagem> SkillsPersonagem { get; set; }
		public List<PersonagemUsuarioEquipamento> Equipamentos { get; set; }

        public long UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        //Atributos calculados juntamente com os buffs e debuffs
        [NotMapped]
        public double AttackAtual { get; set; }
        [NotMapped]
        public double DefenseAtual { get; set; }
        [NotMapped]
        public double WisdomAtual { get; set; }
        [NotMapped]
        public double MagicDefenseAtual { get; set; }
        [NotMapped]
        public double AccuracyAtual { get; set; }
        [NotMapped]
        public double EvadeAtual { get; set; }
        [NotMapped]
        public double SpeedAtual { get; set; }

        //Porcentagem para calcular buffs e debuffs
        [NotMapped]
        public double AttackPorcentagem { get; set; }
        [NotMapped]
        public double DefensePorcentagem { get; set; }
        [NotMapped]
        public double WisdomPorcentagem { get; set; }
        [NotMapped]
        public double MagicDefensePorcentagem { get; set; }
        [NotMapped]
        public double AccuracyPorcentagem { get; set; }
        [NotMapped]
        public double EvadePorcentagem { get; set; }
        [NotMapped]
        public double SpeedPorcentagem { get; set; }
        
        //Lista para guardar os buffs e debuffs do personagem no momento da batalha
        [NotMapped]
        public List<Status> StatusTemporarios { get; set; }

		protected Personagem() { }

		public Personagem(long id, string nome, int level, string descricao, string tipoAtributoAtaquePadrao, int hPBase, int mPBase, int attackBase, int defenseBase, int wisdomBase, int magicDefenseBase, int accuracyBase, int evadeBase, int speedBase, int hP, int mP, int attack, int defense, int wisdom, int magicDefense, int accuracy, int evade, int speed, double hPAtual, double mPAtual, long usuarioId)
		{
			Id = id;
			Nome = nome;
			Level = level;
			Descricao = descricao;
			TipoAtributoAtaquePadrao = tipoAtributoAtaquePadrao;
			HPBase = hPBase;
			MPBase = mPBase;
			AttackBase = attackBase;
			DefenseBase = defenseBase;
			WisdomBase = wisdomBase;
			MagicDefenseBase = magicDefenseBase;
			AccuracyBase = accuracyBase;
			EvadeBase = evadeBase;
			SpeedBase = speedBase;
			HP = hP;
			MP = mP;
			Attack = attack;
			Defense = defense;
			Wisdom = wisdom;
			MagicDefense = magicDefense;
			Accuracy = accuracy;
			Evade = evade;
			Speed = speed;
			HPAtual = hPAtual;
			MPAtual = mPAtual;
			UsuarioId = usuarioId;
		}
	}
}
