﻿using System.Collections.Generic;

namespace Hitochis.Entities.Classes
{
	public class Skill
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string TipoSkill { get; set; }
        public string AreaSkill { get; set; }
        public int? DanoBase { get; set; }
        public int? DanoPorNivel { get; set; }
        public string TipoAtributoAtaque { get; set; }
        public int NivelNecessario { get; set; }
        public double? PorcentagemStatus { get; set; }
        public double? PorcentagemStatusAumentadaPorNivel { get; set; }
        public string AtributoBuff1 { get; set; }
        public string AtributoBuff2 { get; set; }
        public string AtributoBuff3 { get; set; }
        public string AtributoDebuff1 { get; set; }
        public string AtributoDebuff2 { get; set; }
        public string AtributoDebuff3 { get; set; }
		//TODO: Custo de Vida
		//TODO: Custo de Mana
		//TODO: Verificar Cooldown

		public List<SkillPersonagem> SkillsPersonagens { get; set; }

		protected Skill() { }

		public Skill(int id, string nome, string descricao, string tipoSkill, string areaSkill, int? danoBase, int? danoPorNivel, string tipoAtributoAtaque, int nivelNecessario, double? porcentagemStatus, double? porcentagemStatusAumentadaPorNivel, string atributoBuff1, string atributoBuff2, string atributoBuff3, string atributoDebuff1, string atributoDebuff2, string atributoDebuff3)
		{
			Id = id;
			Nome = nome;
			Descricao = descricao;
			TipoSkill = tipoSkill;
			AreaSkill = areaSkill;
			DanoBase = danoBase;
			DanoPorNivel = danoPorNivel;
			TipoAtributoAtaque = tipoAtributoAtaque;
			NivelNecessario = nivelNecessario;
			PorcentagemStatus = porcentagemStatus;
			PorcentagemStatusAumentadaPorNivel = porcentagemStatusAumentadaPorNivel;
			AtributoBuff1 = atributoBuff1;
			AtributoBuff2 = atributoBuff2;
			AtributoBuff3 = atributoBuff3;
			AtributoDebuff1 = atributoDebuff1;
			AtributoDebuff2 = atributoDebuff2;
			AtributoDebuff3 = atributoDebuff3;
		}
	}
}
