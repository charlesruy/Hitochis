﻿using Hitochis.Business;
using Hitochis.Business.Interfaces;
using Hitochis.Data;
using Hitochis.Data.Interfaces;
using Ninject.Modules;

namespace Hitochis.Ninject
{
	public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IUsuarioBusiness<>)).To(typeof(UsuarioBusiness<>));
            Bind(typeof(IUsuarioData<>)).To(typeof(UsuarioData<>));
        }
    }
}
