﻿using Hitochis.Business.Interfaces;
using Hitochis.Data.Interfaces;
using Hitochis.Entities.Classes;
using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace Hitochis.Business
{
	public class UsuarioBusiness<T> : IUsuarioBusiness<T>
    {
        #region | Propriedades |

        private readonly IUsuarioData<Process> _usuarioData;

        #endregion

        #region | Construtor |

        public UsuarioBusiness(IUsuarioData<Process> usuarioData)
	    {
            _usuarioData = usuarioData;
	    }

        #endregion

        #region | Métodos Públicos |

        #region | Criar Conta |

        public Usuario CriarConta(string login, string senha)
        {
            try
            {
                if (String.IsNullOrEmpty(login.Trim()) && String.IsNullOrEmpty(senha.Trim()))
                {
                    throw new Exception("Por favor, digite um login e uma senha..!");
                }
                else if (String.IsNullOrEmpty(login.Trim()))
                {
                    throw new Exception("Por favor, digite um login..!");
                }
                else if (String.IsNullOrEmpty(senha.Trim()))
                {
                    throw new Exception("Por favor, digite uma senha..!");
                }
                else if (senha.Length < 5)
                {
                    throw new Exception("Por favor, digite uma senha com mais de 5 caracteres..!");
                }
                else
                {
                    Usuario newUser = new Usuario(login, CodificarSenha(senha), "FALSE", "0.0", 0);
                    return _usuarioData.CriarConta(newUser);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region | Autenticar |

        public Usuario Autenticar(string login, string senha)
        {
            if (String.IsNullOrEmpty(login.Trim()) && String.IsNullOrEmpty(senha.Trim()))
            {
                throw new Exception("Por favor, digite um login e uma senha..!");
            }
            else
            {

                Usuario user = _usuarioData.Autenticar(login, CodificarSenha(senha));
                if (user != null)
                    return user;
                throw new Exception("Login e/ou Senha inválidos");
            }
        }

        #endregion

        #endregion

        #region | Métodos Privados |

        private string CodificarSenha(string senha)
        {
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            sha1.ComputeHash(ASCIIEncoding.ASCII.GetBytes(senha));
            byte[] sha1Bytes = sha1.Hash;
            StringBuilder sb = new StringBuilder();
            foreach (byte b in sha1Bytes)
            {
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        #endregion
    }
}
