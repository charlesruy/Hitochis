﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class PersonagemMap : EntityTypeConfiguration<Personagem>
	{
		public PersonagemMap()
		{
			ToTable("Personagem");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Nome).HasColumnName("Nome");
			Property(x => x.Level).HasColumnName("Level");
			Property(x => x.Descricao).HasColumnName("Descricao");
			Property(x => x.TipoAtributoAtaquePadrao).HasColumnName("TipoAtributoAtaquePadrao");
			Property(x => x.HPBase).HasColumnName("HPBase");
			Property(x => x.MPBase).HasColumnName("MPBase");
			Property(x => x.AttackBase).HasColumnName("AttackBase");
			Property(x => x.DefenseBase).HasColumnName("DefenseBase");
			Property(x => x.WisdomBase).HasColumnName("WisdomBase");
			Property(x => x.MagicDefenseBase).HasColumnName("MagicDefenseBase");
			Property(x => x.AccuracyBase).HasColumnName("AccuracyBase");
			Property(x => x.EvadeBase).HasColumnName("EvadeBase");
			Property(x => x.SpeedBase).HasColumnName("SpeedBase");
			Property(x => x.HP).HasColumnName("HP");
			Property(x => x.MP).HasColumnName("MP");
			Property(x => x.Attack).HasColumnName("Attack");
			Property(x => x.Defense).HasColumnName("Defense");
			Property(x => x.Wisdom).HasColumnName("Wisdom");
			Property(x => x.MagicDefense).HasColumnName("MagicDefense");
			Property(x => x.Accuracy).HasColumnName("Accuracy");
			Property(x => x.Evade).HasColumnName("Evade");
			Property(x => x.Speed).HasColumnName("Speed");
			Property(x => x.HPAtual).HasColumnName("HPAtual");
			Property(x => x.MPAtual).HasColumnName("MPAtual");

			Property(x => x.UsuarioId).HasColumnName("UsuarioId");
				HasRequired(x => x.Usuario).WithMany(y => y.Personagens).HasForeignKey(x => x.UsuarioId);
		}
	}
}
