﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Hitochis.Entities.HelperClasses
{
	public static class Helper
    {
        public static void ExibirLoading(Control form, Label lbl, string mensagem)
        {
            form.Invoke(new Action(() => lbl.Text = mensagem));
            Thread.Sleep(200);
            for (int j = 0; j < 3; j++)
            {
                form.Invoke(new Action(() => lbl.Text += "."));
                Thread.Sleep(200);
            }
        }

        public static void ExibirMensagem(Control form, Label lbl, string mensagem)
        {
            form.Invoke(new Action(() => lbl.Text = ""));
            for (int i = 0; i < mensagem.Length; i++)
            {
                var conteudo = lbl.Text;
                form.Invoke(new Action(() => lbl.Text = conteudo + mensagem.Substring(i, 1)));
                Thread.Sleep(10);
            }
        }

        public static int Random(int min, int max)
        {
            Random ramdom = new Random();
            return ramdom.Next(min, max);
        }

        public static bool IsNotNull(this object element)
        {
            return (element != null);
        }

        public static bool IsNotNullList<T>(this IEnumerable<T> list)
        {
            return (list.Any() || list.Count() != 0);
        }

        public static bool IsNull(this object element)
        {
            return (element == null);
        }

        public static bool IsNullList<T>(this IEnumerable<T> list)
        {
            return (!list.Any() || list.Count() == 0);
        }
    }
}
