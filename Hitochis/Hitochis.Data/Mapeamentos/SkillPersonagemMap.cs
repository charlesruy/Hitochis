﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class SkillPersonagemMap : EntityTypeConfiguration<SkillPersonagem>
	{
		public SkillPersonagemMap()
		{
			ToTable("SkillPersonagem");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Nivel).HasColumnName("Nivel");

			Property(x => x.SkillId).HasColumnName("SkillId");
			HasRequired(x => x.Skill).WithMany(y => y.SkillsPersonagens).HasForeignKey(x => x.SkillId);

			Property(x => x.PersonagemId).HasColumnName("PersonagemId");
			HasRequired(x => x.Personagem).WithMany(y => y.SkillsPersonagem).HasForeignKey(x => x.PersonagemId);
		}
	}
}
