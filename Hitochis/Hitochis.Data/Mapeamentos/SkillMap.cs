﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class SkillMap : EntityTypeConfiguration<Skill>
	{
		public SkillMap()
		{
			ToTable("Skill");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Nome).HasColumnName("Nome");
			Property(x => x.Descricao).HasColumnName("Descricao");
			Property(x => x.TipoSkill).HasColumnName("TipoSkill");
			Property(x => x.AreaSkill).HasColumnName("AreaSkill");
			Property(x => x.DanoBase).HasColumnName("DanoBase");
			Property(x => x.DanoPorNivel).HasColumnName("DanoPorNivel");
			Property(x => x.TipoAtributoAtaque).HasColumnName("TipoAtributoAtaque");
			Property(x => x.NivelNecessario).HasColumnName("NivelNecessario");
			Property(x => x.PorcentagemStatus).HasColumnName("PorcentagemStatus");
			Property(x => x.PorcentagemStatusAumentadaPorNivel).HasColumnName("PorcentagemStatusAumentadaPorNivel");
			Property(x => x.AtributoBuff1).HasColumnName("AtributoBuff1");
			Property(x => x.AtributoBuff2).HasColumnName("AtributoBuff2");
			Property(x => x.AtributoBuff3).HasColumnName("AtributoBuff3");
			Property(x => x.AtributoDebuff1).HasColumnName("AtributoDebuff1");
			Property(x => x.AtributoDebuff2).HasColumnName("AtributoDebuff2");
			Property(x => x.AtributoDebuff3).HasColumnName("AtributoDebuff3");
		}
	}
}
