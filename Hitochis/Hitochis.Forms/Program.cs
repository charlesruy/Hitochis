﻿using Hitochis.Data;
using Hitochis.Ninject;
using System;
using System.Data.Entity;
using System.Windows.Forms;

namespace Hitochis.Forms
{
	static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Seta o DatabaseFirst para a aplicação
            Database.SetInitializer<HitochisContext>(null);

            //Seta as configurações iniciais para o Ninject
            CompositionRoot.Wire(new ApplicationModule());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(CompositionRoot.Resolve<FormLogin>());
        }
    }
}
