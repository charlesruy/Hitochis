﻿using Hitochis.Entities.Classes;

namespace Hitochis.Data.Interfaces
{
	public interface IUsuarioData<T>
    {
        Usuario CriarConta(Usuario newUser);

        Usuario Autenticar(string login, string senha);
    }
}
