﻿using Hitochis.Entities.Classes;
using System.Data.Entity.ModelConfiguration;

namespace Hitochis.Data.Mapeamentos
{
	public class TipoEquipamentoMap : EntityTypeConfiguration<TipoEquipamento>
	{
		public TipoEquipamentoMap()
		{
			ToTable("TipoEquipamento");

			HasKey(x => x.Id);
			Property(x => x.Id).HasColumnName("Id");

			Property(x => x.Tipo).HasColumnName("Tipo");
		}
	}
}
