﻿namespace Hitochis.Entities.Classes
{
	public class TipoEquipamento
	{
		public int Id { get; set; }
		public string Tipo { get; set; }

		protected TipoEquipamento() { }

		public TipoEquipamento(int id, string tipo)
		{
			Id = id;
			Tipo = tipo;
		}
	}
}
