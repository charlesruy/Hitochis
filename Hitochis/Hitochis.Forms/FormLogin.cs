﻿using Hitochis.Business.Interfaces;
using Hitochis.Entities.Classes;
using Hitochis.Entities.HelperClasses;
using Hitochis.Ninject;
using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace Hitochis.Forms
{
	public partial class FormLogin : Form
    {
        #region | Propriedades |

        private IUsuarioBusiness<Process> _classeBusiness;
        private Usuario USER;

        #endregion

        #region | Construtor |

        public FormLogin(IUsuarioBusiness<Process> classeBusiness)
        {
            _classeBusiness = classeBusiness;
            InitializeComponent();
            btnLoadGame.Focus();
        }

        #endregion

        #region | Métodos |

        #region | New Game |
        private void btnNewGame_Click(object sender, EventArgs e)
        {
            SetarModoDeEntrada("Criar");
            MudarVisibilidadeComponentes();
            txtLogin.Focus();
        }

        #endregion

        #region | Load Game |

        private void btnLoadGame_Click(object sender, EventArgs e)
        {
            SetarModoDeEntrada("Entrar");
            MudarVisibilidadeComponentes();
            txtLogin.Focus();
        }

        #endregion

        #region | Sair |

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region | Voltar |

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            MudarVisibilidadeComponentes();
            btnLoadGame.Focus();
        }

        #endregion

        #region | Entrar |

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            MudarHabilitadoComponentes();
            string login = txtLogin.Text;
            string senha = txtSenha.Text;

            if (btnEntrar.Text == "Criar")
            {
                ThreadsLogin(login, senha, "Criando Conta");                
            }
            else if (btnEntrar.Text == "Entrar")
            {
                ThreadsLogin(login, senha, "Efetuando Login");  
            }
        }

        #endregion

        #endregion

        #region | Métodos Auxiliares |

        private void MudarVisibilidadeComponentes()
        {
            Invoke(new Action(() => btnNewGame.Visible = !btnNewGame.Visible));
            Invoke(new Action(() => btnLoadGame.Visible = !btnLoadGame.Visible));
            Invoke(new Action(() => btnSair.Visible = !btnSair.Visible));

            Invoke(new Action(() => txtLogin.Clear()));
            Invoke(new Action(() => txtSenha.Clear()));
            Invoke(new Action(() => lblLoading.Text = ""));
            Invoke(new Action(() => lblLoading.Visible = false));
            Invoke(new Action(() => txtLogin.Visible = !txtLogin.Visible));
            Invoke(new Action(() => txtSenha.Visible = !txtSenha.Visible));
            Invoke(new Action(() => btnVoltar.Visible = !btnVoltar.Visible));
            Invoke(new Action(() => btnEntrar.Visible = !btnEntrar.Visible));
            Invoke(new Action(() => lblLogin.Visible = !lblLogin.Visible));
            Invoke(new Action(() => lblSenha.Visible = !lblSenha.Visible));
        }

        private void MudarHabilitadoComponentes()
        {
            Invoke(new Action(() => txtLogin.Enabled = !txtLogin.Enabled));
            Invoke(new Action(() => txtSenha.Enabled = !txtSenha.Enabled));
            Invoke(new Action(() => btnVoltar.Enabled = !btnVoltar.Enabled));
            Invoke(new Action(() => btnEntrar.Enabled = !btnEntrar.Enabled));
        }

        private void SetarModoDeEntrada(string acao)
        {
            btnEntrar.Text = acao;
        }

        private void ThreadsLogin(string login, string senha, string acao)
        {
            bool FLAG_READY = false;
            string ERROR_MESSAGE = string.Empty;

            new Thread(() =>
            {
                Invoke(new Action(() => lblLoading.Visible = true));
                while (!FLAG_READY)
                    Helper.ExibirLoading(this, lblLoading, "Loading");
                if (!String.IsNullOrEmpty(ERROR_MESSAGE))
                {
                    Invoke(new Action(() => lblLoading.Text = ERROR_MESSAGE));
                    MudarHabilitadoComponentes();
                    Invoke(new Action(() => txtSenha.Focus()));
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                        Helper.ExibirLoading(this, lblLoading, acao);
                    EntrarNoMenuPrincipal();
                }
            }).Start();

            new Thread(() =>
            {
                try
                {
                    if (acao == "Criando Conta")
                        USER = _classeBusiness.CriarConta(login, senha);
                    else if (acao == "Efetuando Login")
                        USER = _classeBusiness.Autenticar(login, senha);
                    if (USER == null)
                        throw new Exception();
                    FLAG_READY = true;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(500);
                    ERROR_MESSAGE = ex.Message;
                    FLAG_READY = true;
                }
            }).Start();
        }

        private void EntrarNoMenuPrincipal()
        {
            Form formMain = CompositionRoot.Resolve<FormMain>();
            //formMain.InitializeGlobalVars();
            Invoke(new Action(() => this.Hide()));
            Invoke(new Action(() => formMain.ShowDialog()));
            Invoke(new Action(() => this.Show()));
            MudarHabilitadoComponentes();
            MudarVisibilidadeComponentes();
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnEntrar_Click(sender, e);
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            txtSenha.SelectAll();
        }

        private void txtLogin_Enter(object sender, EventArgs e)
        {
            txtLogin.SelectAll();
        }

        #endregion
    }
}
